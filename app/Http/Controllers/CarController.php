<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;
use App\Models\Product;

class CarController extends Controller
{
    public function index($id){
        $productosCliente = Car::where('user_id',$id)->pluck('product_id')->all();
        $productosCarrito = Product::whereIn('id',$productosCliente)->get();
        return $productosCarrito;
    }

    public function save(Request $request){
        $carrito = Car::insert([
            'user_id'=>$request->user_id,
            'product_id'=>$request->product_id
        ]);
        return response()->json($carrito,201);
    }

    public function delete(Request $request){
        $carrito=Car::where('id',$request->id)->delete();
        return response()->json($carrito,204);
    }

}
