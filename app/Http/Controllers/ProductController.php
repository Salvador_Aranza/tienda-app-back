<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Car;

class ProductController extends Controller
{
    public function index()
    {
        $productos= Product::All();
        return response()->json($productos,200);
    }

    public function show($id){
        $producto = Product::find($id);
        return $producto;
    }

    public function save(Request $request){
        $product = Product::create($request->all());
        return response()->json($product,201);
    }

    public function update($id,Request $request){
        $product = Product::find($id);
        $product->name=$request->name;
        $product->description=$request->description;
        $product->save();
        return response()->json($product,200);
    }
    
    public function delete($id){
        /*$productoid = Car::where('product_id',$id)->get();
        if($productoid==null){*/
            $producto= Product::where('id',$id)->delete();
            return response()->json($producto,200);
       /* }
        return response()->json("El producto se encuentra en el carrito del cliente",200);*/
    }
}
