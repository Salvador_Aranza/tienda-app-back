<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Product;
use App\Models\Car;
use App\Http\Controllers;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\API\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products/index',[ProductController::class,'index']);

Route::post('products/save',[ProductController::class,'save']);
Route::get('products/show/{id}',[ProductController::class,'show']);
Route::put('products/update/{id}',[ProductController::class,'update']);
Route::delete('products/delete/{id}',[ProductController::class,'delete']);


Route::get('cars/index/{id}',[CarController::class,'index']);
Route::post('cars/save',[CarController::class,'save']);
Route::delete('cars/delete',[CarController::class,'delete']);


Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);

Route::get('users/{id}',[RegisterController::class, 'users']);
     
/*Route::middleware('auth:api')->group( function () {
    Route::resource('products/index', ProductController::class);
});*/

